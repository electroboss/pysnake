import classes
import pygame
import numpy as np

pygame.init()
screen = pygame.display.set_mode((200,200))
clock = pygame.time.Clock()

snake = classes.Snake(screen, 0, 0, 0)
apple = classes.Apple(screen)

frame = 0

def get_screen(screen,snake,apple):
    screen_data = np.zeros((int(screen.get_size()[0]/10),int(screen.get_size()[1]/10)))
    for segment in snake.segments:
        screen_data[segment] = 2
    screen_data[apple.x,apple.y] = 1
    return screen_data

while 1:
    # events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()

    keys=pygame.key.get_pressed()
    if keys[pygame.K_d] or keys[pygame.K_RIGHT]:
        snake.move(0)
    elif keys[pygame.K_s] or keys[pygame.K_DOWN]:
        snake.move(1)
    elif keys[pygame.K_a] or keys[pygame.K_LEFT]:
        snake.move(2)
    elif keys[pygame.K_w] or keys[pygame.K_UP]:
        snake.move(3)

    if keys[pygame.K_ESCAPE]:
        pygame.quit()
        quit()
    

    # update

    apple_in_snake = snake.check_apple(apple.x,apple.y)
    snake.update(not apple_in_snake)
    if apple_in_snake:
        apple.move()

    if snake.check_alive():
        snake.__init__(screen,0,0,0)
        apple.__init__(screen,snake_segments=snake.segments)

    # draw
    screen.fill((0,0,0))
    apple.draw()
    snake.draw()

    pygame.display.flip()
    clock.tick(10)

    frame += 1
