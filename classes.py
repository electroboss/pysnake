import pygame, random

class Snake():
    def __init__(self,screen,x,y,direction=0,color=(0,255,0),headcolor=(0,255,255)):
        self.screen = screen
        self.segments = [[x,y]]
        self.direction = direction # 0 = right, 1 = down, 2 = left, 3 = up
        self.color = color
        self.headcolor = headcolor
        for x in range(5):
            self.update(remove_segment=False)
    
    def update(self,remove_segment=True):
        # Update segments
        x,y = self.segments[0]
        if self.direction == 0:
            x += 1
        if self.direction == 1:
            y += 1
        if self.direction == 2:
            x -= 1
        if self.direction == 3:
            y -= 1
        self.segments.insert(0,[x,y])
        if remove_segment:
            self.segments.pop()
    
    def draw(self):
        pygame.draw.rect(self.screen,self.headcolor,pygame.Rect(self.segments[0][0]*10,self.segments[0][1]*10,9,9))
        for segment in self.segments[1:]:
            pygame.draw.rect(self.screen,self.color,pygame.Rect(segment[0]*10,segment[1]*10,9,9))

    def move(self, direction):
        if direction != (self.direction + 2) % 4:
            self.direction = direction
    
    def check_alive(self):
        w = int(self.screen.get_size()[0]/10)-1
        h = int(self.screen.get_size()[1]/10)-1
        x, y = self.segments[0]

        if [x,y] in self.segments[1:]:
            return 1
        
        if x > w or x < 0 or y > h or y < 0:
            return 2
    
    def check_apple(self,apple_x,apple_y):
        return [apple_x,apple_y] in self.segments

class Apple():
    def __init__(self,screen,x=None,y=None,width=1,height=1,snake_segments=[],):
        self.screen = screen

        self.x = random.randint(0,int(self.screen.get_size()[0]/10)-1) if x == None else x
        self.y = random.randint(0,int(self.screen.get_size()[1]/10)-1) if y == None else y
        
        while [self.x,self.y] in snake_segments:
            self.x = random.randint(0,int(self.screen.get_size()[0]/10)-1) if x == None else x
            self.y = random.randint(0,int(self.screen.get_size()[1]/10)-1) if y == None else y
        self.width = width
        self.height = height
    
    def draw(self):
        pygame.draw.rect(self.screen,(255,0,0),pygame.Rect(self.x*10,self.y*10,self.height*10-1,self.width*10-1))

    def move(self):
        self.x = random.randint(0,int(self.screen.get_size()[0]/10)-1)
        self.y = random.randint(0,int(self.screen.get_size()[1]/10)-1)